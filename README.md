# Rollup Summary Trigger for Lookup Relationship #
* You can't create a COUNT or SUM roll-up summary field that pulls data from a lookup field without selecting an option to disallow the deletion of the lookup record that is part of the lookup relationship.
* In case of Lookup Relationship we cannot create rollup summary on parent object.
* So we will write the Rollup Summary Trigger for Lookup Relationship .
### Rollup Summary field ###
* A Rollup Summary field is used to calculate the sum of a fields in the child object record.
* It is compulsory to be in a Master-Detail relationship to use the rollup summary.
* A rollup summary field is always created on Master record.
### In this Asset ###
* The relationship between account and contact is lookup with casecade delete that means we cannot create rollup summary field on account which count related contact record so the solution to this are
* Process Builder and Flow.
* Apex Trigger.
* Process Builder will only execute when if a record is created or update but it will fail when the user is deleting record  so we use apex trigger.
