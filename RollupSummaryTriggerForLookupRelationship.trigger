trigger RollupSummaryTriggerForLookupRelationship on Contact (before insert, after insert, before update, after update, after undelete, after delete) {
    if ( Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate ||Trigger.isDelete||Trigger.isUndelete)) {
    RollupSummaryForLookupRHandler.getRecordList(Trigger.old,Trigger.new,Trigger.newMap,Trigger.oldMap);
    }
    if (Trigger.isUndelete ||(Trigger.isBefore && ( Trigger.isInsert || Trigger.isUpdate ))) {
    RollupSummaryForLookupRHandler.findDuplicate(Trigger.old,Trigger.new,Trigger.newMap,Trigger.oldMap);     
}
}